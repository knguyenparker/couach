<?php
/**
 * Frontend Class
 *
 * @package WordPress
 * @subpackage Infinite Loop Theme
 * @since 1.0
 * @author Infinit Loop
 */

if (!defined('ILIO_THEME_VERSION')) exit;

/**
 * @since 1.0
 */
class Ilio_Theme_Frontend extends Master_Common {

    /* --------------------------------------------- */
    /* --------------  DIRECT DISPLAY -------------- */
    /* --------------------------------------------- */

    public function display_main_menu() {
        //$items = f('pub_item');
        //$this->display_template('publicity/items', array('items' => $items));
    }
}