<?php
/**
 * Caching Class
 *
 * @package WordPress
  * @subpackage Infinite Loop BASE - @Plugin Principal
 * @since 1.0
 * @author Infinit Loop
 */

class Ilio_Cache {

	protected $_cache_path;


	public function __construct() {
		// Detect & create temp folder
		$this->_cache_path = ILIO_BASE_CACHING_DIR;
		if (!file_exists($this->_cache_path)) {
			mkdir($this->_cache_path, 0755, true);
		}
	}

	public function get_cache($type = null) {
		if(!$type) {
			return false;
		}
		$file = ILIO_BASE_CACHING_DIR . '/cache.'.$type.'.txt';
		if (file_exists($file)) {
			$cache_content = file_get_contents($file);
		}
		return $cache_content;
	}

	public function update_cache($type = null, $cache_content = null) {
		if(!$type || !$cache_content) {
			return false;
		}
		$file = ILIO_BASE_CACHING_DIR . '/cache.'.$type.'.txt';

		// UPDATE
		if (file_exists($file)) {
			file_put_contents($file, $cache_content);
		}
		// CREATE
		else {
			$fp = fopen($file,"wb");
			fwrite($fp,'');
			fclose($fp);
			file_put_contents($file, $cache_content);
		}
	}

}