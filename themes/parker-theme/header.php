<?php
/**
 * The Header for our theme
 * @package WordPress
 * @subpackage Parker et Parker
 * @since Parker et Parker 1.0
 * @author Philippe BARTOLESCHI - Smart 7
 */
?>
<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<meta http-equiv="imagetoolbar" content="false" />
	<link rel="dns-prefetch" href="//ajax.googleapis.com" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<!--[if lt IE 9]>
	    <script src="<?php echo get_template_directory_uri(); ?>/bower_components/html5shiv/dist/html5shiv.js"></script>
	<![endif]-->

	<?php wp_head(); ?>

	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/bower_components/bootstrap/dist/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/bower_components/components-font-awesome/css/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/bower_components/Hover/css/hover-min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/bower_components/sweetalert/dist/sweetalert.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/app.min.css"/>
</head>
<body <?php body_class(); ?>>

<?php $items = fol('item_menu'); ?>
<header>
	<div class="navbar navbar-default navbar-static-top" id="nav-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle nav-toggle-left" data-toggle="collapse" data-target="#navbar-ex-collapse">
					<span class="sr-only"><?php pll_e('Ouvrir le menu'); ?></span><i class="fa fa-2x fa-fw fa-navicon"></i>
				</button>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-lgn-collapse">
					<span class="sr-only"><?php pll_e('Ouvrir le menu'); ?></span><i class="fa fa-2x fa-fw fa-globe"></i>
				</button>
			</div>
			<div class="col-sm-1 collapse navbar-collapse" id="navbar-ex-collapse">
				<?php if ($items): ?>
					<ul class="nav navbar-nav">
						<?php foreach ($items as $item): ?>
							<li>
								<a href="<?php esc_html_e(get_the_permalink($item['item']->ID)); ?>">
									<?php esc_html_e($item["item"]->post_title); ?>
								</a>
							</li>
						<?php endforeach; ?>
					</ul>
				<?php endif; ?>
			</div>
			<div class="col-sm-1 collapse navbar-collapse" id="navbar-lgn-collapse">
				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<?php echo pll_current_language('slug'); ?> <span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<?php pll_the_languages(array(
								'display_names_as' => 'slug',
								'hide_if_empty' => 0,
								'hide_current'  => 1
							)); ?>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
</header>