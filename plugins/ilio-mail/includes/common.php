<?php
/**
 * Common functions
 *
 * @package    WordPress
 * @subpackage Infinite loop : mail
 * @since      1.0
 * @author     Smart 7
 */

/**
 * Mail function
 *
 */
if(!function_exists('ilio_mail')) {
    function ilio_mail($type = null, $to = null, $data = false, $labels = false, $attachments = array()) {
        $mail_manager = Mail_Manager::get_instance();
        $sendMail = $mail_manager->send_mail($type, $to, $data, $labels, $attachments);
        return $sendMail;
    }
}