<?php
/**
 * Parker et Parker Class Base
 *
 * @package WordPress
 * @subpackage Parker et Parker
 * @since Parker et Parker 1.0
 * @author Philippe BARTOLESCHI - Smart 7
 */

require_once (FUNCTIONS_PATH . 'class.admin.php'); // Edit login screen
require_once (FUNCTIONS_PATH . 'theme.functions.php'); // Include theme functions

/*================================================================================== */
/* Global Theme Base =============================================================== */
class parkerBase {

	public function init() {
		/**
		 *  LANGUAGE & LOCALIZATION / SECURE KEY
		*/
		//load the text domain for localization
		add_action('after_setup_theme', array(&$this,'setupTheme'));

		/*
		 * SCRIPTS
		*/
		add_action( 'init', array(&$this,'enqueueAlertScript'), 0);
		add_action( 'wp_enqueue_scripts', array(&$this,'enqueueScripts'));

		/**
		 *  IMAGES
		*/
		// post thumbnail support
		add_theme_support( 'post-thumbnails' );
		// Remove p tags on images
		add_filter('the_content', array(&$this,'filterPtagsOnImages'));

		/**
		 *  WIDGETS
		*/
		// Unregister default widgets
		add_action('widgets_init', array(&$this,'unregisterDefaultWidgets'));

		/**
		 * EXCERPT LENGTH
		 */
		add_filter('excerpt_length', array(&$this,'atExcerptLength'));
		add_filter('excerpt_more', array(&$this,'atExcerptMore'));
	}

	/**
	 *  Setup Theme
	*/
	function setupTheme() {
		/* Langs */
		$lang_dir = get_template_directory() . '/languages';
		load_theme_textdomain('at', $lang_dir);
		add_theme_support( 'html5', array(
			'gallery', 'caption'
		) );
	}


	/*
	 * SCRIPTS
	*/
	function enqueueAlertScript() {
		wp_enqueue_script('sweet', get_template_directory_uri().'/bower_components/sweetalert/dist/sweetalert.min.js',false,null);
	}

	function enqueueScripts() {
		wp_deregister_script( 'jquery' );

		if(!is_admin()) {
			wp_enqueue_script( 'jquery' , get_template_directory_uri().'/bower_components/jquery/dist/jquery.min.js', false, null); // include jQuery
			wp_enqueue_script('isjs', get_template_directory_uri().'/bower_components/is-js/is.js',false,null,true);
			wp_enqueue_script('flexslider', get_template_directory_uri().'/bower_components/flexslider/jquery.flexslider-min.js',false,null,true);
			wp_enqueue_script('bootstrap', get_template_directory_uri().'/bower_components/bootstrap/dist/js/bootstrap.min.js',false,null,true);

			wp_enqueue_script('jstheme', get_template_directory_uri().'/js/global.min.js',array( 'jquery' ),null,true);

			//wp_localize_script( 'jstheme', 'atVars', $jsVars );
		}
	}

	/**
	 *  IMAGES
	*/
	function filterPtagsOnImages($content){
		return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	}

	/**
	 *  WIDGETS
	*/
	// Unregister default widgets
	function unregisterDefaultWidgets() {
		unregister_widget('WP_Widget_Pages');
		unregister_widget('WP_Widget_Calendar');
		unregister_widget('WP_Widget_Archives');
		unregister_widget('WP_Widget_Links');
		unregister_widget('WP_Widget_Meta');
		unregister_widget('WP_Widget_Categories');
		unregister_widget('WP_Widget_Recent_Posts');
		unregister_widget('WP_Widget_Recent_Comments');
		unregister_widget('WP_Widget_RSS');
		unregister_widget('WP_Widget_Tag_Cloud');
	}

	/**
	 * Adding class to nav menu
	 */
	function addMarkupMenu($output) {
		$output= preg_replace('/menu-item /', 'first-menu-item menu-item', $output, 1);
		$output= substr_replace($output, "last-menu-item menu-item", strripos($output, "menu-item"), strlen("menu-item"));
		return $output;
	}

	/**
	 * This removes the annoying […] to a Read More link
	 */
	function atExcerptMore($more) {
		return '...';
	}

	/**
	 * Set new length for excerpt
	 */
	function atExcerptLength($length) {
		return 20;
	}

}