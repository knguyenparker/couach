/*================================================================================== */
/* Globals Functions =============================================================== */

/* in_array() function */
function inArray(needle, haystack) {
	var length = haystack.length;
	for(var i = 0; i < length; i++) {
		if(haystack[i] == needle) return true;
	}
	return false;
}

function log(e) {
	return console.log(e);
}
/**/

/*================================================================================== */
/* Jquery START ==================================================================== */
jQuery(document).ready(function ($) {
	"use strict";

	// tabs
	$("#tabs").tabs();

	$('.pcma_legend_table').slideUp(0);
	$('.info-sheet-ilio h3').on('click', function() {
		if($(this).next('.pcma_legend_table').hasClass('open')) {
			$(this).next('.pcma_legend_table').slideUp(0).removeClass('open');
		}
		else {
			$(this).next('.pcma_legend_table').slideDown(0).addClass('open');
		}
	});
	
});