<?php

/**
 * Class manager mail
 */
class Mail_Manager
{
    private static $_instance = null;
    protected $_type;
    protected $_to;
    protected $_data;
    protected $_labels;
    protected $_attachments;
    protected $_error;
    protected $_config;

    function __construct() {
    }

    public static function get_instance() {
        if(is_null(self::$_instance)) {
            self::$_instance = new Mail_Manager();  
        }

        return self::$_instance;
    }

    public function send_mail($type = null, $to = null, $data = false, $labels = false, $attachments = array()) {
        if(!$type || !$to) {
            throw new Exception('Erreur de mail. Contacter le développeur.');
        }
        $this->_error = false;
        $this->_type = $type;
        $this->_to = is_array($to) ? array_filter($to) : $to;
        $this->_data = $data;
        $this->_labels = $labels;
        $this->_attachments = $attachments;
        $this->_config = get_option('ilio_settings_emails');

        $mailContent = $this->_get_content_type();
        if($this->_error) {
            return false;
        }

        $subject = $mailContent['title'];
        $body = $mailContent['content'];

        // HEADERS SENDER
        $fromName = isset($this->_config['from_name']) ? $this->_config['from_name'] : false;
        $fromEmail = isset($this->_config['from_email']) ? $this->_config['from_email'] : false;
        if(!$fromName) {
            $fromName = get_bloginfo( 'name' );
        }
        if(!$fromEmail) {
            $fromEmail = get_bloginfo( 'admin_email' );
        }

        $headers[] = 'From: '.$fromName.' <'.$fromEmail.'>';
        $headers[] = 'Content-Type: text/html; charset=UTF-8';

        $body = $this->add_vars_to_mail($body);
                
        ob_start(); 
        include(ILIO_MAIL_FRONTEND_DIR . '/views/body.php'); 
        $message = ob_get_contents(); /* *** */
        ob_end_clean();

        wp_mail( $this->_to, $subject, $message, $headers, $this->_attachments);
        //wpMandrill::mail($this->_to, $subject, $body, '', $this->_attachments);
        return true;
    }

    protected function _get_content_type() {
        $cType = false;
        switch ($this->_type) {
            case 'ilio_user_create':
                $cType = $this->_ilio_user_create();
                break;
            case 'locked_account':
                $cType = $this->_locked_account();
                break;
            case 'unlocked_account':
                $cType = $this->_unlocked_account();
                break;
            case 'recover_password':
                $cType = $this->_recover_password();
                break;
            case 'project':
                $cType = $this->_project();
                break;
            case 'recruitment':
                $cType = $this->_recruitment();
                break;            
            case 'contact':
                $cType = $this->_contact();
                break;
            case 'sale':
                $cType = $this->_sale();
                break;
        default:
                $cType = '_ERROR';
        }
        if($cType == '_ERROR') {
            $this->_error = pll__('Type de contenu non pas pris en charge. Contacter le développeur.');
        }
        return $cType;
    }

    protected function _ilio_user_create() {
        $content = isset($this->_config['ilio_user_create_content']) ? $this->_config['ilio_user_create_content'] : '';
        $title =  isset($this->_config['ilio_user_create_title']) ? $this->_config['ilio_user_create_title'] : '';
        if(!$content || !$title) {
            $this->_error = __('Aucun contenu à envoyer. Merci de vérifier les réglages', 'lgbc');
            return false;
        }
        return array(
            'title' => $title,
            'content' => $content
        );
    }

    protected function _locked_account() {
        $content = isset($this->_config['locked_account_content']) ? $this->_config['locked_account_content'] : '';
        $title =  isset($this->_config['locked_account_title']) ? $this->_config['locked_account_title'] : '';
        if(!$content || !$title) {
            $this->_error = __('Aucun contenu à envoyer. Merci de vérifier les réglages', 'lgbc');
            return false;
        }
        return array(
            'title' => $title,
            'content' => $content
        );
    }

    protected function _unlocked_account() {
        $content = isset($this->_config['unlocked_account_content']) ? $this->_config['unlocked_account_content'] : '';
        $title =  isset($this->_config['unlocked_account_title']) ? $this->_config['unlocked_account_title'] : '';
        if(!$content || !$title) {
            $this->_error = __('Aucun contenu à envoyer. Merci de vérifier les réglages', 'lgbc');
            return false;
        }
        return array(
            'title' => $title,
            'content' => $content
        );
    }

    protected function _recover_password() {
        $content = isset($this->_config['recover_psw_content']) ? $this->_config['recover_psw_content'] : '';
        $title =  isset($this->_config['recover_psw_title']) ? $this->_config['recover_psw_title'] : '';
        if(!$content || !$title) {
            $this->_error = __('Aucun contenu à envoyer. Merci de vérifier les réglages', 'lgbc');
            return false;
        }
        return array(
            'title' => $title,
            'content' => $content
        );
    }

    protected function _project() {
        $content = isset($this->_config['project_content']) ? $this->_config['project_content'] : '';
        $title =  isset($this->_config['project_title']) ? $this->_config['project_title'] : '';
        if(!$content || !$title) {
            $this->_error = __('Aucun contenu à envoyer. Merci de vérifier les réglages', 'lgbc');
            return false;
        }
        return array(
            'title' => $title,
            'content' => $content
        );
    }

    protected function _recruitment() {
        $content = isset($this->_config['recruitment_content']) ? $this->_config['recruitment_content'] : '';
        $title =  isset($this->_config['recruitment_title']) ? $this->_config['recruitment_title'] : '';
        if(!$content || !$title) {
            $this->_error = __('Aucun contenu à envoyer. Merci de vérifier les réglages', 'lgbc');
            return false;
        }
        return array(
            'title' => $title,
            'content' => $content
        );
    }

    protected function _contact() {
        $content = isset($this->_config['contact_content']) ? $this->_config['contact_content'] : '';
        $title =  isset($this->_config['contact_title']) ? $this->_config['contact_title'] : '';
        if(!$content || !$title) {
            $this->_error = __('Aucun contenu à envoyer. Merci de vérifier les réglages', 'lgbc');
            return false;
        }
        return array(
            'title' => $title,
            'content' => $content
        );
    }

    protected function _sale() {
        $content = isset($this->_config['sale_content']) ? $this->_config['sale_content'] : '';
        $title =  isset($this->_config['sale_title']) ? $this->_config['sale_title'] : '';
        if(!$content || !$title) {
            $this->_error = __('Aucun contenu à envoyer. Merci de vérifier les réglages', 'lgbc');
            return false;
        }
        return array(
            'title' => $title,
            'content' => $content
        );
    }

    public function add_vars_to_mail($body) {

        $tags = array();

        /* GLOBALS */
        $tags['SITE-TITLE'] = get_bloginfo( 'name' );
        $tags['SITE-URL'] = home_url( '/' );

        /* ALL FORM FIELDS */
        $tags['ALL-FIELDS'] = '';

        /* USER RELATED */
        $tags['OBJECT'] = '';
        $tags['CIVILITY'] = '';
        $tags['FIRSTNAME'] = '';
        $tags['LASTNAME'] = '';
        $tags['MAIL'] = '';
        $tags['PHONE'] = '';
        $tags['FUNCTION'] = '';
        $tags['SOCIETY'] = '';
        $tags['ACTIVITY'] = '';
        $tags['NUMBERS'] = '';
        $tags['POSTALCODE'] = '';
        $tags['CITY'] = '';
        $tags['COUNTRY'] = '';
        $tags['USERS'] = '';
        $tags['PROJECT'] = '';
        $tags['WEBSITE'] = '';
        $tags['MESSAGE'] = '';
        $tags["CV_url"] = '';
        $tags["CL_url"] = '';


        // $tags['USER-FIRSTNAME'] = '';
        // $tags['USER-LASTNAME'] = '';
        // $tags['USER-USERNAME'] = '';
        // $tags['USER-CIVILITY'] = '';
        // $tags['USER-PSW'] = '';
        // $tags['NEW-USR-PSW'] = '';
        // $tags['USER-EMAIL'] = '';
        // $tags['USER-PHONE'] = '';
        // $tags['USER-MOBILE'] = '';
        // $tags['RECOVER-PSW-URL'] = '';
        // $tags['LINK'] = '';
        // $tags['TITLE'] = '';
        // $tags['USER-RESUME'] = '';
        // $tags['USER-ADDRESS'] = '';
        // $tags['USER-POSTALCODE'] = '';
        // $tags['USER-CITY'] = '';
        // $tags['USER-ROLE'] = '';
        // $tags['USER-SITUATION'] = '';
        // $tags['USER-DOMAINS'] = '';
        // $tags['USER-NATIONALITY'] = '';
        // $tags['MESSAGE'] = '';
        // $tags['FILES'] = '';

        // Fill args with mail data

        $tags['ALL-FIELDS'] = '<ul>';

        foreach ((array)$this->_data as $_kdata => $_data) {
            // Skip empty
            if(!$_data || $_data == "") {
                continue;
            }
            // Skip Status INT
            if($_kdata == 'locked') {
                continue;
            }
            // Skip Password
            if($_kdata == 'psw') {
                continue;
            }

            // PASSWORD CONDITION
            if($_kdata == 'username') {
                if(isset($this->_data['psw']) && $this->_data['psw'] != "") {
                    $tags['ALL-FIELDS'] .= '<li>';
                    if($this->_labels && array_key_exists('psw_change', $this->_labels)) {
                        $tags['ALL-FIELDS'] .= '<strong>'.$this->_labels['psw_change']['label'].' :</strong> ';
                    }
                    $tags['ALL-FIELDS'] .= $this->_data['psw'];
                    $tags['ALL-FIELDS'] .= '</li>';
                }
            }
        }
        $tags['ALL-FIELDS'] .= '</ul>';

        // OBJECT
        if(isset($this->_data['object']) && $this->_data['object'] != "") {
            $tags['OBJECT'] = $this->_data['object'];
        }

        // FIRSTNAME
        if(isset($this->_data['firstname']) && $this->_data['firstname'] != "") {
            $tags['FIRSTNAME'] = $this->_data['firstname'];
        }

        // LASTNAME
        if(isset($this->_data['lastname']) && $this->_data['lastname'] != "") {
            $tags['LASTNAME'] = $this->_data['lastname'];
        }

        // MAIL
        if(isset($this->_data['mail']) && $this->_data['mail'] != "") {
            $tags['MAIL'] = $this->_data['mail'];
        }

        // PHONE
        if(isset($this->_data['phone']) && $this->_data['phone'] != "") {
            $tags['PHONE'] = $this->_data['phone'];
        }

        // FUNCTION
        if(isset($this->_data['function']) && $this->_data['function'] != "") {
            $tags['FUNCTION'] = $this->_data['function'];
        }

        // SOCIETY
        if(isset($this->_data['society']) && $this->_data['society'] != "") {
            $tags['SOCIETY'] = $this->_data['society'];
        }

        // ACTIVITY
        if(isset($this->_data['activity']) && $this->_data['activity'] != "") {
            $tags['ACTIVITY'] = $this->_data['activity'];
        }
        // NUMBERS
        if(isset($this->_data['numbers']) && $this->_data['numbers'] != "") {
            $tags['NUMBERS'] = $this->_data['numbers'];
        }

        // POSTAL CODE
        if(isset($this->_data['postalcode']) && $this->_data['postalcode'] != "") {
            $tags['POSTALCODE'] = $this->_data['postalcode'];
        }
        // CITY
        if(isset($this->_data['city']) && $this->_data['city'] != "") {
            $tags['CITY'] = $this->_data['city'];
        }

        // COUNTRY
        if(isset($this->_data['country']) && $this->_data['country'] != "") {
            $tags['COUNTRY'] = $this->_data['country'];
        }

        // USERS
        if(isset($this->_data['users']) && $this->_data['users'] != "") {
            $tags['USERS'] = $this->_data['users'];
        }

        // PROJECT
        if(isset($this->_data['project']) && $this->_data['project'] != "") {
            $tags['PROJECT'] = $this->_data['project'];
        }

        // WEBSITE
        if(isset($this->_data['website']) && $this->_data['website'] != "") {
            $tags['WEBSITE'] = $this->_data['website'];
        }

        // MESSAGE
        if(isset($this->_data['message']) && $this->_data['message'] != "") {
            $tags['MESSAGE'] = $this->_data['message'];
        }

        // CIVILITY
        if(isset($this->_data['civility']) && $this->_data['civility'] != "") {
            $tags['CIVILITY'] = $this->_data['civility'];
        }

        // CV ATTACHMENT
        if(isset($this->_data['CV_url']) && $this->_data['CV_url'] != "") {
            $tags['CV_url'] = $this->_data['CV_url'];
        }   

        // CL ATTACHMENT
        if(isset($this->_data['CL_url']) && $this->_data['CL_url'] != "") {
            $tags['CL_url'] = $this->_data['CL_url'];
        }

        // REPLACE DYNAMIC DATAS
        foreach ($tags as $tag => $var ) {
            $body = str_replace( '%' . $tag . '%', $var, $body );
        }

        // REPLACE BLOCKQUOTES
        $body = preg_replace('/<blockquote>(.*?)<\/blockquote>/i', '<span style="padding:10px 15px; margin-top:15px; display:block; background-color:#f3f3f3; color:#535961; font-weight:bold;">$1</span>', $body);

        return $body;
    }

}
