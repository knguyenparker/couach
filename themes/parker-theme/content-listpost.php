<article>
    <div class="col-md-6">
        <?php the_date('d F Y'); ?>
        <?php the_post_thumbnail('thumbnail', array( 'alt' => get_the_title(), 'title' => get_the_title())); ?>
    </div>
    <div class="col-md-6">
        <div class="post-title"><?php the_title(); ?></div>
        <p><?php the_excerpt(); ?></p>
        <a href="<?php the_permalink() ?>" class="see-more"><?php _e('Lire plus', 'pkr') ?></a>
    </div>
</article>