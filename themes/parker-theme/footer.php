<?php
/**
 * The template for displaying the footer
 *
 * @package WordPress
 * @subpackage Parker et Parker
 * @since Parker et Parker 1.0
 * @author Philippe BARTOLESCHI - Smart 7
 */
?>

<div class="clearfix"></div>

<ul><?php pll_the_languages();?></ul>

<!--footer start from here-->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-6 footerleft ">
                <div class="footer-title">Logo</div>
                <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                when an unknown printer took a galley.
                </p>
            </div>
            <div class="col-md-offset-1 col-md-6 col-sm-6 paddingtop-bottom">
                <h6 class="footer-title">Menu footer</h6>
                <?php $menu = fol('menu-footer-items'); ?>
                <?php if ($menu): ?>
                    <ul class="footer-menu">
                        <?php foreach ($menu as $item): ?>
                        <li>
                            <a href="<?php echo esc_html_e($item[('link')]) ?>"><?php esc_html_e($item['label']); ?></a>
                        </li>
                        <?php endforeach ?>
                    </ul>
                <?php endif ?>
            </div>
        </div>
    </div>
</footer>

<div class="copyright">
    <div class="container">
        <div class="col-md-6">
            <p>© 2016 - All Rights reserved</p>
        </div>
        <div class="col-md-6">
            <ul class="sub-footer-menu">
                <li><a href="#">webenlance.com</a></li>
                <li><a href="#">About us</a></li>
                <li><a href="#">Blog</a></li>
                <li><a href="#">Faq's</a></li>
                <li><a href="#">Contact us</a></li>
                <li><a href="#">Site Map</a></li>
            </ul>
        </div>
    </div>
</div>

<?php wp_footer(); ?>

</body>
</html>