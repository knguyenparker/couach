<?php
/**
 * Common Functions
 *
 * @package WordPress
 * @subpackage Infinite Loop Theme
 * @since 1.0
 * @author Infinit Loop
 */

if (!defined('ILIO_THEME_VERSION')) exit;

/**
 * Display Main Menu
 *
 */
if(!function_exists('display_main_menu')) {
    function display_main_menu() {
        global $class_theme_display;
        $class_theme_display->display_main_menu();
    }
}