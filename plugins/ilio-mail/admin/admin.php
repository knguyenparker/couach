<?php
/**
 * Admin Plugin Init
 *
 * @package WordPress
 * @subpackage Plugin Infinit Loop - Mail.
 * @since 1.0
 * @author Philippe BARTOLESCHI - Infinit Loop
 */

if (!defined('ILIO_MAIL_VERSION')) exit;

/**
 * @since 1.0
 */
class InfinitLoopMailAdminInit extends Master_Common {

    /**
     * PHP5 constructor method.
     *
     * @since 1.0
     */
    public function __construct() {
        /* Admin Email Menu */
        add_action( 'admin_menu', array( &$this, 'ilio_email_menu' ) );
        /* Admin Scripts */
        add_action('admin_enqueue_scripts', array( &$this, 'ilio_admin_enqueue_styles' ) );
        add_action('admin_enqueue_scripts',  array( &$this, 'ilio_admin_enqueue_scripts' ) );
    }

    /**
     * Create email settings menu
     *
     * @since 1.0
     */
    public function ilio_email_menu() {
        /* Mail manager */
        $current_user = wp_get_current_user();
        if(sizeof($current_user->roles) > 0) {
            if($current_user->roles[0] == "administrator") {

                add_menu_page(
                    __('Mails'),
                    __('Mails'),
                    'edit_posts',
                    'ilio_mail',
                    array(&$this,'email_action')
                );
            }
        }
    }

    /**
     * Loads the admin JS files.
     *
     * @since 1.0
     */
    public function ilio_admin_enqueue_scripts() {
        if (isset($_GET['page'])) {
            if (preg_match('/ilio_mail/', $_GET['page'])) {
                wp_enqueue_script( 'jquery-ui-core' );
                wp_enqueue_script( 'jquery-ui-tabs' );
                wp_enqueue_script('iliomail', ILIO_MAIL_ADMIN_URL . '/js/ilio-mail.js', array('jquery'), ILIO_MAIL_VERSION, true);
                
            }
        }
    }

    /**
     * Loads the admin CSS files.
     *
     * @since 1.0
     */
    public function ilio_admin_enqueue_styles() {
        if (isset($_GET['page'])) {
            if (preg_match('/ilio_mail/', $_GET['page'])) {
                wp_enqueue_script( 'jquery-ui-core' );
                wp_enqueue_script( 'jquery-ui-tabs' );
                wp_enqueue_style('ilio-admin', ILIO_MAIL_ADMIN_URL . '/css/ilio_styles.css', array(), ILIO_MAIL_VERSION, 'all');
                wp_enqueue_style( 'ilio_pg-ui-theme', ILIO_MAIL_ADMIN_URL.'/css/ui-wp-theme/jquery-ui-1.8.17.custom.css', 999);
            }
        }
    }

    /**
     * Emails Settings Save Action
     *
     * @since 1.0
     */
    public function email_action() {
        $message = '';
        if(isset($_POST['ilio_email_submit'])) {
            $_POST = stripslashes_deep($_POST);
            $mails_settings = $_POST['email_setting'];
            // XIVO : RECRUTEMENT
            $mails_settings['recruitment_content'] = $_POST['recruitment_content'];
            // XIVO : DEVENIR REVENDEUR
            $mails_settings['sale_content'] = $_POST['sale_content'];
            // XIVO : SOUMETTRE UN PROJET
            $mails_settings['project_content'] = $_POST['project_content'];
            // XIVO : CONTACT
            $mails_settings['contact_content'] = $_POST['contact_content'];

            if(!get_option('ilio_settings_emails')) { add_option('ilio_settings_emails', '', '', 'yes'); }
            update_option('ilio_settings_emails', $mails_settings);

            $message = pll__('Réglages mis à jour');

        }

        /* INCLUDE EXPORT PAGE */
        $this->display_template('settings_emails', array('message' => $message));
    }
}

$infinitloop_mail_admin = new InfinitLoopMailAdminInit();
