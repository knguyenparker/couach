require('es6-promise').polyfill();

var   gulp 		   = require('gulp')
    , sass 		   = require('gulp-sass')
    , plumber      = require('gulp-plumber')
    , cssmin       = require('gulp-cssmin')
    , rename       = require('gulp-rename')
    , jsmin        = require('gulp-jsmin')
    , sourcemaps   = require('gulp-sourcemaps')
    , spritesmith  = require('gulp.spritesmith')
    , browserify   = require('gulp-browserify')
    , autoprefixer = require('gulp-autoprefixer');

var basePath = './themes/parker-theme/';
var configuration =
{
    'spriteDirectory' : basePath + 'images/sprites/**/*.png',
    'jsDirectory'     : basePath + 'js/global.js',
    'sassDirectory'   : basePath + 'scss/**/*.scss'
};

gulp.task('css', function()
{
    gulp.src(configuration.sassDirectory)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(cssmin())
        .pipe(rename({ suffix : '.min' }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(basePath + 'css'));
});

gulp.task('js', function()
{
    gulp.src(configuration.jsDirectory)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(browserify())
        .pipe(jsmin())
        .pipe(rename({ suffix : '.min' }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(basePath + 'js'));
});

gulp.task('sprite', function() {
    var datas = gulp.src(configuration.spriteDirectory)
        .pipe(spritesmith({
            retinaSrcFilter: [basePath + 'images/sprites/**/*@2x.png'],
            retinaImgName: 'sprite@2x.png',
            imgName : 'images/sprite.png',
            cssName : 'scss/helpers/_sprite.scss'
        }));

    datas.img.pipe(gulp.dest(basePath));
    datas.css.pipe(gulp.dest(basePath));
});

gulp.task('default', ['css', 'js', 'sprite'], function()
{
    gulp.watch([basePath + 'scss/**/*.scss'], ['css']);
    gulp.watch([basePath + 'js/global.js'], ['js']);
    gulp.watch([basePath + 'images/sprites/**/*.png'], ['sprite']);
});