<?php
/**
 * Emails Settings Page
 *
 * @package WordPress
 * @subpackage SILINET - @Plugin Principal
 * @since 1.0
 * @author Philippe BARTOLESCHI - Parker et Parker
 */
?>
<?php if (!defined('ILIO_MAIL_VERSION')) exit; ?>
<?php
$o = get_option('ilio_settings_emails');
$o = maybe_unserialize($o);
$args_wpeditor = array('textarea_rows'=> 10, 'media_buttons' => false, 'teeny' => false);
?>
<div class="wrap no_move lcwp_form">
    <h2 class="ilio-main-title">
        <span class="ilio-ilio-title"><?php echo __('Gestion des emails', 'lilio'); ?></span>
    </h2>

    <?php if (strlen($message)) : ?>
        <div id="message" class="updated below-h2">
            <p><?php echo $message; ?></p>
        </div>
    <?php endif; ?>

    <div id="tabs">
        <form name="pg_admin" id="ilioEmailForm" method="post" class="form-wrap ilio-form-validate ilio-form-fields" action="">

            <ul class="tabNavigation">
                <li><a href="#main_opt"><?php _e('Général', 'lilio') ?></a></li>
<!--                 <li><a href="#users_opt"><?php //_e('Utilisateur', 'lilio') ?></a></li>-->                
                <li><a href="#admin_opt"><?php _e('Administrateur', 'lilio') ?></a></li>
            </ul>

            <div id="main_opt">
                <h3><?php _e("Données Email", 'lilio'); ?></h3>
                <?php
                $from_name = $o['from_name'] ? sanitize_text_field($o['from_name']) : get_bloginfo( 'name' );
                $from_mail = $o['from_mail'] ? sanitize_text_field($o['from_mail']) : get_bloginfo( 'admin_email' );
                ?>
                <table class="widefat pg_table">
                    <tr>
                        <td class="pg_label_td"><?php _e("Nom d'envoi", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <input type="text" name="email_setting[from_name]" value="<?php echo $from_name; ?>" class="required" />
                        </td>
                        <td><span class="info"><?php _e('Le nom visible dans le champ "De" de l\'email', 'lilio'); ?></span></td>
                    </tr>

                    <tr>
                        <td class="pg_label_td"><?php _e("Email d'envoi", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <input type="text" name="email_setting[from_mail]" value="<?php echo $from_mail; ?>" class="required validate-email" />
                        </td>
                        <td><span class="info"><?php _e('L\'email visible dans le champ "De" de l\'email', 'lilio'); ?></span></td>
                    </tr>

                </table>
            </div>

            <div id="users_opt">
                <div class="postbox-ilio info-sheet-ilio">
                    <div class="inside">
                        <h3><?php _e("Variables utilisables", 'lilio'); ?></h3>
                        <table class="pcma_legend_table">
                            <tbody>
                            <tr>
                                <td style="width: 180px;">%SITE-TITLE%</td>
                                <td><span class="info"><?php echo __('Titre du site', 'lilio'); ?></span></td>
                            </tr>
                            <tr>
                                <td style="width: 180px;">%SITE-URL%</td>
                                <td><span class="info"><?php echo __('URL du site', 'lilio'); ?></span></td>
                            </tr>
                            <tr>
                                <td style="width: 180px;">%ALL-FIELDS%</td>
                                <td><span class="info"><?php echo __('Toutes les données utilisateur', 'lilio'); ?></span></td>
                            </tr>
                            <tr>
                                <td style="width: 180px;">%USER-USERNAME%</td>
                                <td><span class="info"><?php echo __('Identifiant utilisateur', 'lilio'); ?></span></td>
                            </tr>
                            <tr>
                                <td style="width: 180px;">%USER-PSW%</td>
                                <td><span class="info">
										<?php echo __('Mot de passe utilisateur', 'lilio'); ?>
                                        <small style="color: #D54E21;"> <strong>(<?php echo __('Uniquement lors d\'une mise à jour', 'lilio'); ?>)</strong></small>
									</span></td>
                            </tr>
                            <tr>
                                <td style="width: 180px;">%USER-FIRSTNAME%</td>
                                <td><span class="info"><?php echo __('Prénom utilisateur', 'lilio'); ?></span></td>
                            </tr>
                            <tr>
                                <td style="width: 180px;">%USER-LASTNAME%</td>
                                <td><span class="info"><?php echo __('Nom utilisateur', 'lilio'); ?></span></td>
                            </tr>
                            <tr>
                                <td style="width: 180px;">%USER-EMAIL%</td>
                                <td><span class="info"><?php echo __('Email utilisateur', 'lilio'); ?></span></td>
                            </tr>
                            <tr>
                                <td style="width: 180px;">%USER-PHONE%</td>
                                <td><span class="info"><?php echo __('Téléphone utilisateur', 'lilio'); ?></span></td>
                            </tr>
                            <tr>
                                <td style="width: 180px;">%USER-MOBILE%</td>
                                <td><span class="info"><?php echo __('Mobile utilisateur', 'lilio'); ?></span></td>
                            </tr>
                            <tr>
                                <td style="width: 180px;">%RECOVER-PSW-URL%</td>
                                <td><span class="info">
										<?php echo __('URL de récupération du mot de passe', 'lilio'); ?>
                                        <small style="color: #D54E21;"> <strong>(<?php echo __('Uniquement lors d\'une demande utilisateur', 'lilio'); ?>)</strong></small>
									</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 180px;">%LINK%</td>
                                <td><span class="info"><?php echo __('Lien envoyé', 'lilio'); ?></span></td>
                            </tr>
                            <tr>
                                <td style="width: 180px;">%TITLE%</td>
                                <td><span class="info"><?php echo __('Titre envoyé', 'lilio'); ?></span></td>
                            </tr>
                            <tr>
                                <td style="width: 180px;">%USER-RESUME%</td>
                                <td><span class="info"><?php echo __('CV envoyé', 'lilio'); ?></span></td>
                            </tr>

                            <tr>
                                <td style="width: 180px;">%FORMATIONS%</td>
                                <td><span class="info"><?php echo __('Liste formations (Formations à la carte)', 'lilio'); ?></span></td>
                            </tr>

                            <tr>
                                <td style="width: 180px;">%OFFER%</td>
                                <td><span class="info"><?php echo __('Nom de l\'offre RH', 'lilio'); ?></span></td>
                            </tr>

                            <tr>
                                <td style="width: 180px;">%COMPANY%</td>
                                <td><span class="info"><?php echo __('Société de l\'utilisateur', 'lilio'); ?></span></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

<!--                 <h3><?php _e("Nouveau compte", 'lilio'); ?></h3>
                <table class="widefat pg_table">
                    <tr>
                        <td class="pg_label_td"><?php _e("Titre de l'email", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <input type="text" name="email_setting[ilio_user_create_title]" value="<?php echo sanitize_text_field($o['ilio_user_create_title']); ?>" class="required" />
                        </td>
                    </tr>
                    <tr>
                        <td class="pg_label_td"><?php _e("Contenu de l'email", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <?php echo wp_editor( $o['ilio_user_create_content'], 'ilio_user_create_content', $args_wpeditor); ?>
                        </td>
                    </tr>
                </table>

                <h3><?php _e("Mot de passe oublié", 'lilio'); ?></h3>
                <table class="widefat pg_table">
                    <tr>
                        <td class="pg_label_td"><?php _e("Titre de l'email", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <input type="text" name="email_setting[recover_psw_title]" value="<?php echo sanitize_text_field($o['recover_psw_title']); ?>" class="required" />
                        </td>
                    </tr>
                    <tr>
                        <td class="pg_label_td"><?php _e("Contenu de l'email", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <?php echo wp_editor( $o['recover_psw_content'], 'recover_psw_content', $args_wpeditor); ?>
                        </td>
                    </tr>
                </table>

                <h3><?php _e("Mise à jour de mot de passe", 'lilio'); ?></h3>
                <table class="widefat pg_table">
                    <tr>
                        <td class="pg_label_td"><?php _e("Titre de l'email", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <input type="text" name="email_setting[update_pwd_title]" value="<?php echo sanitize_text_field($o['update_pwd_title']); ?>" class="required" />
                        </td>
                    </tr>
                    <tr>
                        <td class="pg_label_td"><?php _e("Contenu de l'email", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <?php echo wp_editor( $o['update_pwd_content'], 'update_pwd_content', $args_wpeditor); ?>
                        </td>
                    </tr>
                </table>

                <h3><?php _e("Compte débloqué", 'lilio'); ?></h3>
                <table class="widefat pg_table">
                    <tr>
                        <td class="pg_label_td"><?php _e("Titre de l'email", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <input type="text" name="email_setting[unlocked_account_title]" value="<?php echo sanitize_text_field($o['unlocked_account_title']); ?>" class="required" />
                        </td>
                    </tr>
                    <tr>
                        <td class="pg_label_td"><?php _e("Contenu de l'email", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <?php echo wp_editor( $o['unlocked_account_content'], 'unlocked_account_content', $args_wpeditor); ?>
                        </td>
                    </tr>
                </table>

                <h3><?php _e("Utilisateur refusé", 'lilio'); ?></h3>
                <table class="widefat pg_table">
                    <tr>
                        <td class="pg_label_td"><?php _e("Titre de l'email", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <input type="text" name="email_setting[refused_account_title]" value="<?php echo (isset($o['refused_account_title'])) ? sanitize_text_field($o['refused_account_title']) : ''; ?>" class="required" />
                        </td>
                    </tr>
                    <tr>
                        <td class="pg_label_td"><?php _e("Contenu de l'email", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <?php echo wp_editor( (isset($o['refused_account_content'])) ? $o['refused_account_content'] : '', 'refused_account_content', $args_wpeditor); ?>
                        </td>
                    </tr>
                </table>

            </div> -->
            <div id="admin_opt">

                <?php // Mail formulaire de contact ?>
                <h3><?php _e("Formulaire de contact", 'lilio'); ?></h3>
                <table class="widefat pg_table">
                    <tr>
                        <td class="pg_label_td"><?php _e("Titre de l'email", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <input type="text" name="email_setting[contact_title]" value="<?php echo sanitize_text_field($o['contact_title']); ?>" class="required" />
                        </td>
                    </tr>
                    <tr>
                        <td class="pg_label_td"><?php _e("Destinataire", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <input type="text" name="email_setting[contact_contact]" value="<?php echo sanitize_text_field($o['contact_contact']); ?>" class="required" />
                        </td>
                    </tr>
                    <tr>
                        <td class="pg_label_td"><?php _e("Contenu de l'email", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <?php echo wp_editor( $o['contact_content'], 'contact_content', $args_wpeditor); ?>
                        </td>
                    </tr>
                </table>

                <?php // Mail soumettre un projet ?>
                <h3><?php _e("Soumettre un projet", 'lilio'); ?></h3>
                <table class="widefat pg_table">
                    <tr>
                        <td class="pg_label_td"><?php _e("Titre de l'email", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <input type="text" name="email_setting[project_title]" value="<?php echo sanitize_text_field($o['project_title']); ?>" class="required" />
                        </td>
                    </tr>
                    <tr>
                        <td class="pg_label_td"><?php _e("Destinataire", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <input type="text" name="email_setting[project_contact]" value="<?php echo sanitize_text_field($o['project_contact']); ?>" class="required" />
                        </td>
                    </tr>
                    <tr>
                        <td class="pg_label_td"><?php _e("Contenu de l'email", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <?php echo wp_editor( $o['project_content'], 'project_content', $args_wpeditor); ?>
                        </td>
                    </tr>
                </table>

                <?php // Mail Devenir revendeur ?>
                <h3><?php _e("Devenir revendeur", 'lilio'); ?></h3>
                <table class="widefat pg_table">
                    <tr>
                        <td class="pg_label_td"><?php _e("Titre de l'email", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <input type="text" name="email_setting[sale_title]" value="<?php echo sanitize_text_field($o['sale_title']); ?>" class="required" />
                        </td>
                    </tr>
                    <tr>
                        <td class="pg_label_td"><?php _e("Destinataire", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <input type="text" name="email_setting[sale_contact]" value="<?php echo sanitize_text_field($o['sale_contact']); ?>" class="required" />
                        </td>
                    </tr>
                    <tr>
                        <td class="pg_label_td"><?php _e("Contenu de l'email", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <?php echo wp_editor( $o['sale_content'], 'sale_content', $args_wpeditor); ?>
                        </td>
                    </tr>
                </table>

                <?php // Mail recrutement ?>
                <h3><?php _e("Recrutement candidature spontanée", 'lilio'); ?></h3>
                <table class="widefat pg_table">
                    <tr>
                        <td class="pg_label_td"><?php _e("Titre de l'email", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <input type="text" name="email_setting[recruitment_title]" value="<?php echo sanitize_text_field($o['recruitment_title']); ?>" class="required" />
                        </td>
                    </tr>
                    <tr>
                        <td class="pg_label_td"><?php _e("Destinataire", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <input type="text" name="email_setting[recruitment_contact]" value="<?php echo sanitize_text_field($o['recruitment_contact']); ?>" class="required" />
                        </td>
                    </tr>
                    <tr>
                        <td class="pg_label_td"><?php _e("Contenu de l'email", 'lilio'); ?></td>
                        <td class="pg_field_td">
                            <?php echo wp_editor( $o['recruitment_content'], 'recruitment_content', $args_wpeditor); ?>
                        </td>
                    </tr>
                </table>
            </div>

            <input type="submit" name="ilio_email_submit" value="<?php _e('Enregistrer les réglages', 'lilio') ?>" class="button-primary" />
        </form>
    </div>
</div>
