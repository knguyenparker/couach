<?php
/**
 * @package WordPress
 * @subpackage Parker et Parker
 * @since Parker et Parker 1.0
 * @author Philippe BARTOLESCHI - Smart 7
 */
?>
<?php get_header(); ?>

<section class="content">
    <div class="container">
    	<h1><?php _e('Listing actu', 'pkr') ?></h1>

        <?php if ( have_posts() ) : ?>

            <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part( 'content', 'listpost' ); ?>
            <?php endwhile; ?>

            <?php echo pkr_paging_nav(); ?>

        <?php else : ?>
            <?php _e('Aucun article disponible', 'pkr') ?>
        <?php endif; ?>


</section>

<?php get_footer(); ?>