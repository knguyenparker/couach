<?php
    $mainMenu = fo('items_main_menu');
?>
<ul>
    <?php foreach ($mainMenu as $item): ?>
    <li><a href="<?php echo $item['page_option_lang']; ?>"><?php echo $item['text_option_lang']; ?></a></li>
    <?php endforeach ?>
</ul>