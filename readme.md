#Readme

## How to use this repository ?

* Clone it
* Remove .git in this new folder (rm -Rf ./.git)
* On a shell, launch composer (composer install) locate on this folder
* Configure wp-config.php in root
* Create your vhost (like website.local)
* Install all package in package.json with npm (launch command "npm i" to install all dependancies)
* Got to http://myhost.local/core/
* Install Wordpress
* Go on admin http://myhost.local/core/wp-admin
* Change (in settings tab) Site Address (URL) by removing "/core" in, you should have now http://myhost.local insteed of http://myhost.local/core
* Go to appearence > themes, select your theme
* Disconnect yourself from Admin zone, reconnect you (the modification of site Address seems to create some sessions issues)
* ???
* Profit

## Troubleshooting

If the theme didn't work, check your logs (/var/logs/apaches2/***.log by default)

Parker's theme got some polylang function by default, so it can crash on one of it quickly if you didn't actived this plugin.